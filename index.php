<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");

echo "Name : " .$sheep->name. "<br>"; // "shaun"
echo "Legs : " .$sheep->legs. "<br>"; // 4
echo "Cold Blooded: " .$sheep->cold_blooded. "<br>"; // "no"


$kodok = new Frog("buduk");

echo "<br> Name : " .$kodok->name. "<br>"; // "buduk"
echo "Legs : " .$kodok->legs. "<br>"; // 4
echo "Cold Blooded: " .$kodok->cold_blooded. "<br>"; // "no"
echo $kodok->jump() . "<br>"; // "hop hop"


$sungokong = new Ape("kera sakti");

echo "<br> Name : " .$sungokong->name. "<br>"; // "buduk"
echo "Legs : " .$sungokong->legs. "<br>"; // 4
echo "Cold Blooded: " .$sungokong->cold_blooded. "<br>"; // "no"
echo $sungokong->yell(); // "Auooo"

?>